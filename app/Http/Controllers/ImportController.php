<?php

namespace App\Http\Controllers;

use Mail;
use Session;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class ImportController extends Controller
{
	public function parseImport(Request $request)
	{
		$validator = Validator::make($request->all(), [
				'csv_file'  => 'required|mimes:csv,txt|max:2048',
				'html_file'  => 'required|mimes:html'
		]);
		
		if($validator->fails()) {
			return Redirect::back()->withErrors($validator)->withInput($request->all());
		}
		
		// HTML FILE UPLOAD
		$html_file = $request->html_file;
		$name = time().'.'.$html_file->getClientOriginalExtension();
		$destinationPath = public_path('/uploads/templates');
		$html_file->move($destinationPath, $name);
		
		// CSV FILE UPLOAD
		$csv_file = $request->csv_file;
		$csv_file_name = time().'.'.$csv_file->getClientOriginalExtension();
		$destinationPath = public_path('/uploads/csv');
		$csv_file->move($destinationPath, $csv_file_name);
		
		try {
			$emails = array();
			$filename = public_path('/uploads/csv/' . $csv_file_name);
			
			// Reading csv file
          	$file = fopen($filename,"r");
			$index = 0;
			while (($filedata = fgetcsv($file, 1000, ",")) !== FALSE) {
				$num = count($filedata );
				for ($c=0; $c < $num; $c++) {
					$emails[$index] = $filedata[$c];
				}
				$index++;
			}
			fclose($file);
			
			// Send Mail
			if(count($emails) > 0) {
				$htmlfilename = public_path('/uploads/templates/' . $name);
				$data = array();
				$data["content"] = file_get_contents($htmlfilename);
				Mail::send("emails.emails", $data, function($message) use ($emails){    
					$message->from(env('MAIL_FROM_ADDRESS', 'suman@gmail.com'), 'Sample Mail')->to($emails)->subject('Sample Email');    
				});
				\Session::flash('success', 'Mail sent successfully.');
			} else {
				\Session::flash('error', 'No emails found in the csv file.');
			}
            return Redirect::back();
        } catch (\Exception $e) {
            \Session::flash('error', $e->getMessage());
            return Redirect::back();
        }
    }
	
}
