@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-body">
						@if (Session::has('success'))
							<div class="alert alert-success text-white alert_bg"><strong>{{ Session::get('success') }}</strong></div>
						@endif
						@if (Session::has('error'))
						   <div class="alert alert-danger text-white alert_bg"><strong>{{ Session::get('error') }}</strong></div>
						@endif
                        <form class="form-horizontal" method="POST" action="{{ route('import_parse') }}" enctype="multipart/form-data">
                            @csrf
							
							<div class="form-group{{ $errors->has('html_file') ? ' has-error' : '' }}">
                                <label for="html_file" class="col-md-4 control-label">Email template (HTML file)</label>

                                <div class="col-md-6">
                                    <input id="html_file" type="file" class="form-control" name="html_file" required accept="text/html" style="padding-bottom: 35px;" />

                                    @if ($errors->has('html_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('html_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group{{ $errors->has('csv_file') ? ' has-error' : '' }}">
                                <label for="csv_file" class="col-md-4 control-label">Emails list (CSV file)</label>

                                <div class="col-md-6">
                                    <input id="csv_file" type="file" class="form-control" name="csv_file" required accept=".csv, application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" style="padding-bottom: 35px;" />

                                    @if ($errors->has('csv_file'))
                                        <span class="help-block">
                                        <strong>{{ $errors->first('csv_file') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-8 col-md-offset-4">
                                    <button type="submit" class="btn btn-primary">
                                        Submit
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection